from app import app_flask_instance, db
from app.models import User, Post, Comment

@app_flask_instance.shell_context_processor
def make_shell_context():
    return {'db': db, 'User':User, 'Post': Post, 'Comment': Comment}
