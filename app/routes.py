from flask import render_template, flash, redirect, url_for, request, g
from flask_babel import get_locale
from app import app_flask_instance, db
from app.forms import LoginForms, RegisterationForm, EditProfileForm, PostForm, PostComment
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Post, Comment
from werkzeug.urls import url_parse
from datetime import datetime



@app_flask_instance.route('/', methods= ['GET','POST'])
@app_flask_instance.route('/index', methods= ['GET','POST'])
@login_required
def index():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(body=form.post.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your post is now live!')
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page, app_flask_instance.config['POSTS_PER_PAGE'], False)
    next_url = url_for('index',page = posts.next_num)\
    if posts.has_next else None
    prev_url = url_for('index',page = posts.prev_num)\
    if posts.has_prev else None
    return render_template("index.html", title='Home Page', form=form,
                           posts=posts.items, next_url=next_url, prev_url=prev_url)


@app_flask_instance.route('/login', methods = ['GET' , 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForms()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app_flask_instance.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app_flask_instance.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app_flask_instance.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page, app_flask_instance.config['POSTS_PER_PAGE'], False)
    next_url = url_for('user', username=user.username, page = posts.next_num)\
        if posts.has_next else None
    prev_url = url_for('user', username=user.username, page = posts.prev_num)\
        if posts.has_prev else None
    return render_template('user.html', user=user, posts=posts.items, next_url=next_url, prev_url=prev_url)

@app_flask_instance.before_request
def before_request():
    g.locale = str(get_locale())
    if current_user.is_authenticated:
        print('now before request')
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@app_flask_instance.route('/editprofile', methods =['GET','POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('your changes has been saved')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile', form=form)

@app_flask_instance.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following {}!'.format(username))
    return redirect(url_for('user', username=username))

@app_flask_instance.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('user', username=username))


@app_flask_instance.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, app_flask_instance.config['POSTS_PER_PAGE'], False)
    next_url = url_for('explore',page = posts.next_num)\
        if posts.has_next else None
    prev_url = url_for('explore',page = posts.prev_num)\
        if posts.has_prev else None
    return render_template('index.html', title='Explore', posts=posts.items, next_url=next_url, prev_url=prev_url)

@app_flask_instance.route('/post_detail/<post_id>', methods = ['GET','POST'])
@login_required
def post_detail(post_id):
    post = Post.query.filter_by(id=post_id).first()
    form = PostComment()
    print(request.form)
    if form.validate_on_submit():
        print("in form")
        comment = Comment(body=form.comment.data, comment_by=current_user, on_post=post)
        db.session.add(comment)
        db.session.commit()
        return redirect(url_for('post_detail', post_id=post_id))
    return render_template('detail.html', title="post detail", post =post, form=form)