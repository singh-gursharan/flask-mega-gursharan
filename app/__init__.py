from flask import Flask, request
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_babel import Babel

app_flask_instance = Flask(__name__)
app_flask_instance.config.from_object(Config)
db = SQLAlchemy(app_flask_instance)
migrate = Migrate(app_flask_instance, db)
login = LoginManager(app_flask_instance)
login.login_view = 'login'
bootstrap = Bootstrap(app_flask_instance)
moment = Moment(app_flask_instance)
babel = Babel(app_flask_instance)


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(app_flask_instance.config['LANGUAGES'])

from logging.handlers import RotatingFileHandler
import logging
import os

if not app_flask_instance.debug:
    # ...

    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.DEBUG)
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.DEBUG)
    log.addHandler(file_handler)
    log.info('new logger by werkzeug')
    app_flask_instance.logger.addHandler(file_handler)

    app_flask_instance.logger.setLevel(logging.DEBUG)
    app_flask_instance.logger.info('Microblog startup')




from app import routes, models, errors
