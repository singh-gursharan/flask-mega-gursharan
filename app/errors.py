from flask import render_template
from app import app_flask_instance
from app import db

@app_flask_instance.errorhandler(404)
def errorhandler(error):
    return render_template('404.html'), 404

@app_flask_instance.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('505.html'), 500
